package com.ftd.services.erospublisher.bl.impl;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.ftd.eapi.message.service.v1.*;
import com.ftd.services.erospublisher.api.request.ServiceRequest;
import com.ftd.services.erospublisher.api.response.ServiceResponse;
import com.ftd.services.erospublisher.util.ErosSoapClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBElement;

@Slf4j
@Service(value = "FloristService")
public class ErosPublisherImpl implements com.ftd.services.erospublisher.bl.ErosPublisher {

    @Autowired
    private ErosSoapClient erosSoapClient;

    @Override
    @Timed
    @ExceptionMetered
    public ServiceResponse postMessage(ServiceRequest serviceRequest, String siteId) {
        log.info("postMessage siteId ({})", siteId);

        PostMessage postMessage = new PostMessage();
        postMessage.setErosMessage(serviceRequest.getErosMessage());
        postMessage.setMainMemberCode(serviceRequest.getFloristId());

        ObjectFactory factory = new ObjectFactory();
        JAXBElement<PostMessage> postMessageJAXBElement = factory.createPostMessage(postMessage);
        JAXBElement<PostMessageResponse> postMessageResponseJAXBElement = (JAXBElement<PostMessageResponse>)erosSoapClient.call(postMessageJAXBElement,serviceRequest.getFloristId());
        PostMessageResponse postMessageResponse = postMessageResponseJAXBElement.getValue();
        SingleMessageResponse singleMessageResponse = postMessageResponse.getSingleMessageResponse();

        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.setProcessedErosMessage(singleMessageResponse.getProcessedErosMessage());
        serviceResponse.setResponseCode(singleMessageResponse.getResponseCode().getCode());

        return serviceResponse;
    }
}
