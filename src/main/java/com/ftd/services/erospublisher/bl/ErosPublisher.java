package com.ftd.services.erospublisher.bl;

import com.ftd.services.erospublisher.api.request.ServiceRequest;
import com.ftd.services.erospublisher.api.response.ServiceResponse;

public interface ErosPublisher {
    ServiceResponse postMessage(ServiceRequest request, String siteId);
}
