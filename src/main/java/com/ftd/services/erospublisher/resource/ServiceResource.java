package com.ftd.services.erospublisher.resource;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.ftd.services.erospublisher.api.request.ServiceRequest;
import com.ftd.services.erospublisher.api.response.ServiceResponse;
import com.ftd.services.erospublisher.bl.ErosPublisher;
import io.swagger.annotations.ApiImplicitParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@RestController
@RequestMapping("/{siteId}/api")
public class ServiceResource {

    @Autowired
    private ErosPublisher erosPublisher;

    @RequestMapping(value = "/postMessage", method = RequestMethod.POST)
    @ApiImplicitParam(name = "siteId",
            value = "Valid examples siteId=[proflowers, ftd]",
            required = true,
            dataType = "string",
            paramType = "query")
    @Timed
    @ExceptionMetered
    public ServiceResponse postMessage(@RequestBody ServiceRequest serviceRequest,
                                             @PathVariable String siteId) {
        log.info("postMessage: {} {}", siteId, serviceRequest.getSiteId());
        return erosPublisher.postMessage(serviceRequest, siteId);
    }

}


