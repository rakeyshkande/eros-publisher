package com.ftd.services.erospublisher.api.response;

import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceResponse implements Serializable {

    private ProcessedErosMessage processedErosMessage;
    private String responseCode;
    private String message;
    private String type;

}
