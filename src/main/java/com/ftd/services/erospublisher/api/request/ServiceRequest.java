package com.ftd.services.erospublisher.api.request;

import com.ftd.eapi.message.service.v1.ErosMessage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceRequest {

    private String messageType;
    private String comments;
    private SiteId siteId;
    private String floristId;
    private ErosMessage erosMessage;

}
