package com.ftd.services.erospublisher.config;

public class GlobalConstants {

    public static final String SERVICE_NAME = "florist-fulfillment-gateway";

    public static final String MESSAGE_TYPE_FTD = "FTD Order";
    public static final String MESSAGE_TYPE_CAN = "Cancel Order";
    public static final String MESSAGE_TYPE_ASK = "Ask Message";
    public static final String MESSAGE_TYPE_ANS = "Answer Message";
    public static final String MESSAGE_TYPE_GEN = "General Message";
    public static final String MESSAGE_TYPE_REJ = "Reject Order";
    public static final String MESSAGE_TYPE_CON = "Confirm Cancel";
    public static final String MESSAGE_TYPE_DEN = "Deny Cancel";

    public static final String WEBGIFTS_SOURCE_CODE = "WEBGIFTS";
    public static final String WEBGIFTS_ORDER_CHANNEL = "Webgifts";
    public static final String WEBGIFTS_FULFILLMENT_CHANNEL = "dropship";
    public static final String WEBGIFTS_SERVICE_LEVEL = "Next-Day";
    public static final String WEBGIFTS_PRODUCT_TYPE = "specialtyGift";
    public static final String WEBGIFTS_DELIVERY_TYPE = "Domestic";

    public static final String OGS_LANGUAGE_ID = "en-US";
    public static final String OGS_ORDER_TYPE = "New";
    public static final String OGS_ORDER_TOTAL = "orderTotal";
    public static final String OGS_DISCOUNT_AMOUNT = "discountAmount";
    public static final String OGS_COUNTRY_CODE_US = "US";
    public static final String OGS_COUNTRY_CODE_CA = "CA";
    public static final String OGS_CURRENCY_CODE = "USD";
    public static final String OGS_PAYMENT_TYPE_NO_PAYMENT = "noPayment";
    public static final String OGS_DEFAULT_LAST_NAME = "N/A";
    public static final String OGS_RECIPIENT_ADDRESS_TYPE = "Home";
    public static final String OGS_AVS_PERFORMED = "avsPerformed";
    public static final String OGS_RETAIL_PRODUCT_AMOUNT = "retailProductAmount";
    public static final String OGS_SALE_PRODUCT_AMOUNT = "saleProductAmount";
    public static final String OGS_TAX_AMOUNT = "taxAmount";
    public static final String OGS_ITEM_TOTAL = "itemTotal";

    public static final String ZERO_DOLLAR_AMOUNT = "0.00";
    public static final String VERIFIED = "VERIFIED";
    public static final String SUCCESS = "Success";

}
