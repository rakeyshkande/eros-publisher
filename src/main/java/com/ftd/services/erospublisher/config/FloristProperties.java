package com.ftd.services.erospublisher.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "configurations")
public class FloristProperties {
    private String scheduleDelay;
    private List<FloristConfiguration> floristConfiguration;

    @Getter
    @Setter
    public static class FloristConfiguration {
        private String floristId;
        private String siteId;
        private String userId;
        private String password;
    }

}
