package com.services.micro.template.demo.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ftd.services.erospublisher.api.request.ServiceRequest;
import com.ftd.services.erospublisher.resource.ServiceResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@RunWith(SpringRunner.class)
public class ServiceResourceTest {
    private MockMvc mockMvc;

    @Mock
    private com.ftd.services.erospublisher.bl.ErosPublisher erosPublisher;

    @InjectMocks
    private ServiceResource serviceResource;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(serviceResource)
                .build();
    }

    @Test
    public void testString() throws Exception {

        ServiceRequest request = new ServiceRequest();
        request.setMessageType("GEN");
        request.setComments("hello world");
        //request.setSiteId("ftd");

        mockMvc.perform(
                post("/ftd/api/postMessage")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request))
        ).andExpect(status().is2xxSuccessful());

        //verify(erosPublisher).postMessage(request, "ftd");
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}